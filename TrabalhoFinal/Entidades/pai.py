class Pai:
  
  def __init__(self,identificador = 0, altura = 0, nacionalidade = "", cor = "", peso = 0):
    self._altura = altura
    self._nacionalidade = nacionalidade
    self._cor = cor
    self._peso = peso

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def altura(self):
    return self._altura

  @altura.setter
  def altura(self,altura):
    self._altura = altura

  @property
  def nacionalidade(self):
    return self._nacionalidade

  @nacionalidade.setter
  def nacionalidade(self,nacionalidade):
    self._nacionalidade = nacionalidade

  @property
  def cor(self):
    return self._cor

  @cor.setter
  def cor(self,cor):
    self._cor = cor

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self,peso):
    self._peso = peso

  def CordoOlho(self):
    print("Verde")

  def CordoCabelo(self):
    print("Loiro")