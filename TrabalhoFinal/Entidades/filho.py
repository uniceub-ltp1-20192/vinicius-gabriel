from Entidades.pai import Pai

class Filho(Pai):
  def __init__(self,CordoCabelo = "Loiro"):
    super().__init__()
    self._CordoCabelo = CordoCabelo


  @property
  def CordoCabelo(self):
    return self._CordoCabelo

  @CordoCabelo.setter
  def CordoCabelo(self,CordoCabelo):
    self._CordoCabelo = CordoCabelo

  def CordoOlho(self):
    print("Castanho Claro")
   
    # self._tamanho = tamanho
    # self._raca = raca
    # self._cor = cor
    # self._peso = peso

  def __str__(self):
    return """Dados de Pug:
  Identificador: {} 
  Tamanho: {}
  Raca: {}
  Cor: {}
  Peso: {}
  Cor do Cabelo: {}
    """.format(self.identificador
    ,self.tamanho,
    self.raca,
    self.cor,
    self.peso,
    self.CordoCabelo)
