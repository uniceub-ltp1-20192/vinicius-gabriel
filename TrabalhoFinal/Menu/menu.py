from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.filho import Filho

class Menu:
    @staticmethod
    def menuPrincipal():
        print("""
            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Deletar 
            """)
        return Validador.validar("[0-4]",
        """Opcao do menu deve estar entre {}""",
        """Opcao {} Valida!""")
        

    @staticmethod
    def menuConsultar():
        return input("""
                0 - Voltar
                1 - Consultar por identificador
                2 - Consultar por propriedade
                    """)

    @staticmethod
    def iniciarMenu():
        opMenu = ""
        d = Dados()
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        voltar = Menu.menuBuscaPorIdentificador(d)
                        if voltar != None:
                            print(voltar)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "2":
                        voltar = Menu.menuBuscarPorAtributo(d)
                        if voltar != None:
                            print(voltar)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")  
                opMenu = ""            
            elif opMenu == "2":
                print("Entrei em Inserir")
                Menu.menuInserir(d)
                opMenu = ""
            elif opMenu == "3":
                print("Entrei em Alterar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        voltar = Menu.menuBuscaPorIdentificador(d)
                        if voltar != None:
                            Menu.menuAlterar(voltar,d)
                        else:
                            print("Nao encontrado")

                    elif opMenu == "2":
                        voltar = Menu.menuBuscarPorAtributo(d)
                        if voltar != None:
                            Menu.menuAlterar(voltar,d)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = ""
            elif opMenu == "4":
                print("Entrei em Deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        voltar = Menu.menuBuscaPorIdentificador(d)
                        Menu.menuDeletar(d,voltar)
                        
                    elif opMenu == "2":
                        voltar = Menu.menuBuscarPorAtributo(d)
                        Menu.menuDeletar(d,voltar)
                        
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!") 
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            else:
                print("Digite uma opcao valida!")

    @staticmethod
    def menuInserir(d):   
        filho = Filho()    
        filho.peso = input("Informe um peso:")
        filho.cor = input("Informe uma cor:")
        filho.altura = input("Informe a altura:")
        filho.CordoOlho = input("Informe a Cor do Olho:")
        filho.nacionalidade = input("Informa a Nacionalidade:")
        d.inserirDado(filho)

    @staticmethod
    def menuAlterar(voltar,d):   
        # Crie um validador para receber o valor de cada entrada
        # e nao alterar caso o valor informado seja vazio 
        # "" ou None
        print(voltar)
        voltar.peso = Validador.validarValorEntrada(voltar.peso,"Informe um peso:") 
        voltar.cor =  Validador.validarValorEntrada(voltar.cor,"Informe uma cor:") 
        voltar.altura = Validador.validarValorEntrada(voltar.tamanho,"Informe a altura:") 
        voltar.CordoOlho = Validador.validarValorEntrada(voltar.tipoDeRabo,"Informe a Cor do Olho:") 
        voltar.nacionalidade = Validador.validarValorEntrada(voltar.raca,"Informa a Nacionalidade:") 
      
        d.alterarDado(voltar)


    @staticmethod
    def menuDeletar(d,entidade):
        print(entidade)
        resposta = input(
            """Deseja deletar o filho?
            S - Sim
            N - Nao""")
        if(resposta == "S" or  resposta == "s"):
            d.deletar(entidade)


    @staticmethod
    def menuBuscaPorIdentificador(d):        
        voltar = d.buscarPorIdentificador(
            Validador.validar(r'\d+','',''))
        return voltar

    @staticmethod
    def menuBuscarPorAtributo(d : Dados):
        voltar = d.buscarPorAtributo(
            input("Informe um tamanho"))
        print(voltar)
