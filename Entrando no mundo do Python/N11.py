print("Exercício 11.1\n")
'''Escolha algum dos programas que você fez sobre
listas, e adicione algumas linhas ao final do programa para que
ele faça o seguinte:
• Imprima a seguinte mensagem: “Os primeiros 3 itens da
lista são: ”. E use um slice para imprimir os três
primeiros itens da lista do programa escolhido.
• Imprima a seguinte mensagem: “Os últimos 3 itens da
lista são: ”. E use um slice para imprimir os três
últimos itens da lista do programa escolhido.
• Imprima a seguinte mensagem: “Os 3 itens no meio da
lista são: ”. E use um slice para imprimir os três itens
no meio da lista do programa escolhido.'''

transport = ["Honda", "BMW", "Jetta", "Civic", "Maverick"]
print("Os primeiros 3 itens são: {}.".format(transport[:3]))
print("Os 3 últimos itens são: {}.".format(transport[2:]))
print("Os 3 itens no meio são: {}.".format(transport[1:4]))

print("\n----------------------------------------------")

print("Exercício 11.2\n")
'''Crie uma lista de sabores de pizza e armazene em pizza_hut.
Faça uma cópia da lista de pizzas e armazene em
pizza_hot_paraguai. E faça o que se pede:
• Adiciona uma pizza em pizza_hut.
• Adicione uma pizza diferente em pizza_hot_paraguai.
• Demonstre que a pizza hut original é diferente da
cópia paraguaia!'''

pizza_hut = ["Marguerita", "Portuguesa", "Quatro Queijos"]
pizza_hot_paraguai = pizza_hut[:]
pizza_hut.append("Calabresa")
pizza_hot_paraguai.append("Chocolate")
print(pizza_hut)
print(pizza_hot_paraguai)

print("\n----------------------------------------------")

print("Exercício 11.3\n")
'''Todos os códigos nessa nota de aula
imprimiram as listas sem o uso de loops. Escolha um código dessa
nota e imprima todos os itens como um cardápio. Use um loop apra
cada lista.'''

pizza_hut = ["Marguerita", "Portuguesa", "Quatro Queijos"]
pizza_hot_paraguai = pizza_hut[:]
pizza_hut.append("Calabresa")
pizza_hot_paraguai.append("Chocolate")
for pizza_hut in pizza_hut:
    print(pizza_hut)

print("\n")

for pizza_hot_paraguai in pizza_hot_paraguai:
    print(pizza_hot_paraguai)

print("\n----------------------------------------------")
