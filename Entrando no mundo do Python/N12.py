print("Exercício 12.1\n")
'''Um buffet oferece 5 tipos de comida. Pense em 5 tipos de
comida e as guarde em um tupla.
• Use um loop for para imprimir cada comida que o restaurante
oferece.'''

comidas = ("Uvas Itália", "Patê de ervas finas", "Salada Caprese", "Queijos", "Mini pães")
for comidas in comidas:
    print(comidas)

print("\n----------------------------------------------")

print("Exercício 12.2\n")
'''Tente modificar um dos itens, e garanta que o python rejeita
a alteração.'''

comidas = ("Uvas Itália", "Patê de ervas finas", "Salada Caprese", "Queijos", "Mini pães")
for comidas in comidas:
    print(comidas)
comidas[4] = ("Doce")
print(comidas)

print("\n----------------------------------------------")

print("Exercício 12.3\n")
'''O restaurante mudou o menu, trocando dois pratos do cardápio.
Adicione um bloco de código que rescreve a tupla, e então
reusa o loop for para imprimir os itens do novo menu.'''

comidas = ("Uvas Itália", "Patê de ervas finas", "Salada Caprese", "Queijos", "Mini pães")
for comidas in comidas:
    print(comidas)

comidas = ("Uvas Itália", "Patê de ervas finas", "Bolo", "Queijos", "Vinho")
print("\nNovos Pratos: ")
for comidas in comidas:
    print(comidas)

print("\n----------------------------------------------")
