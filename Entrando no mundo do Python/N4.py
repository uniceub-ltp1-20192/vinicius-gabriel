print("Exercício 4.1\n")
'''Armazene o nome de uma pessoa e inclua alguns
caracteres de espaço em branco no início e no final do
nome. Certifique­se de usar cada combinação de
caracteres, "\t" e "\n", pelo menos uma vez. Imprima o
nome uma vez, para que o espaço em branco ao redor do
nome seja exibido. Em seguida, imprima o nome usando cada
uma das três funções de DECAPAGEM (stripping) lstrip(),
rstrip() e strip().'''

name = "                 Marcos\tFelipe\n            "
print(name)
print("==================")

# Tira os espaços em branco do lado esquerdo
print(name.lstrip())
print("==================")

# Tira os espaços em branco do lado esquerdo
print(name.rstrip())
print("==================")

# Tira os espaços de toda a String
print(name.strip())

print("\n----------------------------------------------------------")

print("Exercício 4.2\n")
'''Solicite uma frase ao usuário e apresente quantos
caracteres existem na frase sem contar os espaços em
branco.'''

name = input("Digite seu nome completo: ")
print("Quantidade de letras do seu nome é:", len(name.replace(" ", "")), "Letras")

print("\n----------------------------------------------------------")
