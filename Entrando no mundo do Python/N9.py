print("Exercício 9.1\n")
'''Implemente um código em Python que some todos
elementos dentro de uma lista.'''

addition = [5, 7, 12]
print(sum(addition))

print("\n----------------------------------------------")

print("Exercício 9.2\n")
'''Implemente um código em Python que multiplique todos
elementos dentro de uma lista.'''

import numpy

multiplication = [1, 2, 3, 4, 5]
result = numpy.prod(multiplication)
print(result)

print("\n----------------------------------------------")

print("Exercício 9.3\n")
'''Escreva um código que retorne o menor elemento de
uma lista.'''

food = ["Feijão", "Batata", "Arroz"]
print(min(food))

print("\n----------------------------------------------")

print("Exercício 9.4\n")
'''Escreva um código que fornecidas duas listas imprima
uma lista com os elementos comuns entre elas.
Exemplo: ListaA = [“ABC”, “DEF”, “GHI”, “YWZ”]
ListaB = [“CEB”, “YWZ”, “ABC”]
Saída: [“ABC”, “YWZ”]'''

ListA = ["ABC", "DEF", "GHI", "YWZ"]
ListB = ["CEB", "YWZ", "ABC"]
result = list(set(ListA) & set(ListB))
print(result)

print("\n----------------------------------------------")

print("Exercício 9.5\n")
'''Implemente um programa que imprima todos elementos de
uma lista em ordem alfabética'''

names = ["Marcelo", "Cristina", "Ana", "Fernando"]
names.sort()
print(names)

print("\n----------------------------------------------")

print("Exercício 9.6\n")
'''Implemente um código que imprima uma lista com o
tamanho de cada elemento da lista de entrada.
Exemplo: lista = [“cebola”, “carro”, “oi”, “mansões”]
Saída: [6,5,2,7]'''

print("\n----------------------------------------------")

print("Exercício 9.7\n")
'''Implemente um programa que fornecida uma lista de
palavras e uma palavra indique quais elementos da lista
são anagramas da palavra.'''

print("\n----------------------------------------------")
