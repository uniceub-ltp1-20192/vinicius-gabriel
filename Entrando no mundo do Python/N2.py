print("Exercício 2.1\n")
'''Crie um programa que defina duas variáveis, quantidade_cebolas
e preco_cebola, atribua valores inteiros para essas variáveis.
Imprima o valor de quantidade_cebolas muliplicado pelo
preco_cebola'''

quantidade_cebolas = 10
preco_cebola = 5
total = 10 * 5
print("Você irá pagar R${} pelas cebolas".format(total))

print("\n--------------------------------------------------------------------------------------------------")

print("Exercício 2.2\n")
'''Faça um comentário em português em cada linha do programa
abaixo explicando para você mesmo que elas fazem. Execute o
programa abaixo, corrigindo os erros.'''
# Quantidade total de Cebolas

cebolas = 300

# Quantidade de Cebolas na Caixa
cebolas_na_caixa = 120

# Espaço livre nas Caixas
espaco_caixa = 5

# Quantidade total de Caixas
caixas = 60

# Quantidade de Cebolas fora das caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

# Quantidade de Caixas Vazias
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa)

# Quantidade de Caixas que serão necessárias para guardar todas as cebolas
caixas_necessarias = (cebolas_fora_da_caixa / espaco_caixa)

print("Existem {} cebolas encaixotadas".format(cebolas_na_caixa))
print("Existem {} cebolas sem caixa".format(cebolas_fora_da_caixa))
print("Em cada caixa cabem {} cebolas".format(espaco_caixa))
print("Ainda temos {} caixas vazias".format(caixas_vazias))
print("Então, precisamos de {} caixas para empacotar todas as cebolas".format(caixas_necessarias))

print("\n--------------------------------------------------------------------------------------------------")

print("Exercício 2.3\n")
'''Brinque com os valores das variáveis do programa 2.2 e veja o
que acontece com a saída do seu programa.'''
#Quantidade total de Cebolas

cebolas = int(input("Quantas Cebolas ao total você tem: "))

#Quantidade de Cebolas na Caixa
cebolas_na_caixa = int(input("Quantas Cebolas você tem dentro das caixas: "))

#Espaço livre nas Caixas
espaco_caixa = int(input("Quantas Cebolas cabem em cada caixa: "))

#Quantidade total de Caixas
caixas = int(input("Quantas Caixas você tem ao todo: "))

#Quantidade de Cebolas fora das Caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

#Quantidade de Caixas Vazias
caixas_vazias = caixas - (cebolas_na_caixa / espaco_caixa)

#Quantidade de Caixas que serão necessárias para guardar todas as cebolas
caixas_necessarias = (cebolas_fora_da_caixa / espaco_caixa)

print("Existem {} cebolas encaixotadas.".format(cebolas_na_caixa))
print("Existem {} cebolas sem caixa".format(cebolas_fora_da_caixa))
print("Em cada caixa cabem {} cebolas.".format(espaco_caixa))
print("Ainda tem {} caixas vazias.".format(caixas_vazias))
print("Então, precisa de {} caixas para empacotar todas as cebolas.".format(caixas_necessarias))

print("\n--------------------------------------------------------------------------------------------------")
