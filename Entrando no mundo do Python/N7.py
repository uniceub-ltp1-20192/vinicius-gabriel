print("Exercício 7.1\n")
'''Se você pudesse convidar alguém, vivo ou morto, para
jantar, quem você convidaria? Faça uma lista que inclua
pelo menos três pessoas que você gostaria de convidar
para o jantar. Em seguida, use sua lista para imprimir
uma mensagem para cada pessoa, convidando­a para jantar.'''

names = ["joão", "marcela", "vanessa", "felipe"]

# Mensagem convidando todos para o jantar
for names in names:
    print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names.title()))

print("\n----------------------------------------------")

print("Exercício 7.2\n")
'''Acabou de ouvir que um dos seus convidados não pode
fazer o jantar, por isso precisa de enviar um novo
conjunto de convites. Você terá que pensar em outra
pessoa para convidar.
• Comece com o seu programa do exercício 7.1. Adicione
uma declaração de impressão no final do programa,
informando o nome do convidado que não pode
comparecer.
• Modifique sua lista, substituindo o nome do
convidado que não pode comparecer com o nome da nova
pessoa que você está convidando.
• Imprima um segundo conjunto de mensagens de convite,
uma para cada pessoa que ainda esteja na sua lista.'''

names = ["joão", "marcela", "vanessa", "felipe"]

# Mensagem convidando todos para o jantar
print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names[0].title()))
print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names[1].title()))
print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names[2].title()))
print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names[3].title()))
print("\n")

# Dizendo que um convidado não irá
print("O {} não estará presente no jantar que acontecerá no sábado!".format(names[3].title()))
print("\n")

# Adicionando pessoa a lista
names[3] = "thiago"
print("\n")

# Mensagem atualizada convidando todos para o jantar
for names in names:
    print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names.title()))

print("\n----------------------------------------------")

print("Exercício 7.3\n")
'''Você acabou de encontrar uma mesa de jantar maior,
então agora há mais espaço disponível. Pense em mais três
convidados para convidar para o jantar.
• Comece com o seu programa no Exercício 7.2. Adicione
uma mensagem de impressão ao final do programa
informando às pessoas que você encontrou uma mesa de
jantar maior
• Use insert() para adicionar um novo convidado ao
início de sua lista.
• Use insert () para adicionar um novo convidado ao
meio da sua lista.
• Use append() para adicionar um novo convidado ao
final da sua lista.
• Imprima um novo conjunto de mensagens de convite,
uma para cada pessoa na sua lista.'''

names = ["joão", "marcela", "vanessa", "felipe"]
names1 = names[:]

# Mensagem convidando todos para o jantar
for names1 in names1:
    print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names1.title()))
print("\n")

# Dizendo que um convidado não irá
print("O {} não estará presente no jantar que acontecerá no sábado!".format(names[3].title()))
print("\n")

names[3] = "thiago"
print("\n")

# Mensagem atualizada convidando todos para o jantar
names2 = names[:]
for names2 in names2:
   print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names2.title()))
print("\n")

names3 = names[:]

# Mensagem informando que agora terá mais pessoas presentes no jantar
for names3 in names3:
   print("Olá, {}, agora nós temos uma mesa maior, e com isso mas pessoas estarão presente em nosso jantar!".format(names3.title()))
print("\n")

# Adicionando nomes a lista
names.insert(0, 'milena')
names.insert(3, 'geovana')
names.append('erick')

names4 = names[:]

# Mensagem atualizada convidando todos para o jantar
for names4 in names4:
   print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names4.title()))

print("\n----------------------------------------------")

print("Exercício 7.4\n")
'''Você acabou de descobrir que sua nova mesa de jantar
não chegará a tempo para o jantar e terá espaço para
apenas dois convidados.
• Comece com seu programa do Exercício 7.3. Adicione
uma nova linha que imprima uma mensagem dizendo que
você pode convidar apenas duas pessoas para o
jantar.
• Use pop() para remover os convidados da sua lista,
um de cada vez, até que apenas dois nomes permaneçam
na sua lista. Sempre que você inserir um nome na sua
lista, imprima uma mensagem para essa pessoa,
informando­o de que não pode convidá­lo para jantar.
• Imprima uma mensagem para cada uma das duas pessoas
que ainda estão na sua lista, informando­as de que
ainda estão convidadas.
• Use del para remover os dois últimos nomes da sua
lista, para que você tenha uma lista vazia. Imprima
sua lista para ter certeza de que você realmente tem
uma lista vazia no final do seu programa.'''

names = ["joão", "marcela", "vanessa", "felipe"]
names1 = names[:]
for names1 in names1:
   print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names1.title()))
print("\n")

# Dizendo que um convidado não irá
print("O {} não estará presente no jantar que acontecerá no sábado!".format(names[3].title()))
print("\n")

# Adicionando convidado a lista
names[3] = "thiago"
print("\n")

# Mensagem atualizada convidando todos para o jantar
names2 = names[:]
for names2 in names2:
   print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names2.title()))
print("\n")

names3 = names[:]
print("Olá pessoal, agora nós temos uma mesa maior, e com isso mas pessoas estarão presente em nosso jantar!")
print("\n")

# Adicionando nomes a lista
names.insert(0, 'milena')
names.insert(3, 'geovana')
names.append('erick')

names4 = names[:]

# Mensagem atualizada convidando todos para o jantar
for names4 in names4:
   print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names4.title()))
print("\n")

print("No jantar de sábado só poderá ter 2 convidados presentes!")
print("\n")

# Mensagem desmarcando o jantar com alguns convidados
names5 = names[:]
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
print("\n")

print("Olá {}, você está convidado para o jantar".format(names[0].title()))
print("Olá {}, você está convidado para o jantar".format(names[1].title()))

# Tirando nomes restantes da lista
del names5[0]
del names5[0]
print(names5)

print("\n----------------------------------------------")
