print("Exercício 1.1\n")
'''Escreva um programa que imprima na tela “Já é meu
segundo programa”'''

print("Já é meu segundo programa")

print("\n------------------------------")

print("Exercício 1.2\n")
'''Escreva um programa que imprima na tela um menu com
três opções: 1 ­ cadastrar, 2 – logar e 3 – sair, cada
opção e uma linha.'''

print("Cadastrar\n Logar\n Sair")

print("\n------------------------------")

print("Exercício 1.3\n")
'''Escreva um programa que imprima os números pares de 1
até 10.'''

print("2 4 6 8 10")

print("\n------------------------------")
