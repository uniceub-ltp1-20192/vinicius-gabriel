print("Exercício 10.1\n")
'''Use o loop for para imprimir os números de 1 a 20'''

for numbers in range(1, 21):
   print(numbers)

print("\n----------------------------------------------")

print("Exercício 10.2\n")
'''Faça uma lista de de 1 a um milhão e depois imprima
cada um dos valores da lista.'''

numbers = list(range(1, 1000001))
for n in numbers:
   print(n)

print("\n----------------------------------------------")

print("Exercício 10.3\n")
'''Faça uma lista de de 1 a um milhão e use as funções
min() e max() para grantir que sua lista realmente começa
em 1 e termina em um milhão. Depois utilize a função
sum() para imprimir a soma dos valores da lista.'''

numbers = list(range(1, 1000001))
print(min(numbers))
print(max(numbers))
print(sum(numbers))

print("\n----------------------------------------------")

print("Exercício 10.4\n")
'''Use o terceiro parâmetro de função range para fazer
uma lista com os números impares de 1 a 20. Imprima cada
elemento da lista separadamente.'''

impa = list(range(1, 21, 2))
for n in impa:
   print(n)

print("\n----------------------------------------------")

print("Exercício 10.5\n")
'''Faça uma lista com todos os números múltiplos de 3,
no intervalo de 3 a 1000. Imprima cada valor
separadamente.'''

multiplos_de_tres = list(range(3, 1000, 3))
for n in multiplos_de_tres:
   print(n)

print("\n----------------------------------------------")

print("Exercício 10.6\n")
'''Um número elevado a terceira potência e chamado de
cubo. Faça uma lista com todos os cubos entre 1 e 100,
imprima cada valor separadamente'''

for n in range(1, 101):
   n = n ** 3
   print(n)

print("\n----------------------------------------------")

print("Exercício 10.7\n")
'''Faça compreensão de lista para gerar a lista do
exercício 10.6'''

terceira_potencia = [n ** 3 for n in range(1, 101)]
print(terceira_potencia)

print("\n----------------------------------------------")
