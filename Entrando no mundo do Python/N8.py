print("Exercício 8.1\n")
'''Pense em pelo menos cinco lugares no mundo que você
gostaria de visitar.
• Armazene os locais em uma lista. Certifique­se
de que a lista não esteja em ordem alfabética.
• Imprima sua lista na ordem original. Não se
preocupe em imprimir a lista item por item,
basta imprimi­la como uma lista bruta do Python.
• Use sorted() para imprimir sua lista em ordem
alfabética sem modificar a lista atual.
• Mostre que sua lista ainda está em sua ordem
original, imprimindo­a.
• Use sorted() para imprimir sua lista em ordem
alfabética inversa sem alterar a ordem da lista
original.
• Mostre que sua lista ainda está em sua ordem
original, imprimindo­a.
• Use reverse() para alterar a ordem da sua lista.
Imprima a lista para mostrar que a ordem foi
alterada.
• Use reverse() para alterar a ordem da sua lista
novamente. Imprima a lista para mostrar que ela
está de volta ao pedido original.
• Use sort() para que a lista seja armazenada em
ordem alfabética. Imprima a lista para mostrar
que a ordem foi alterada.
• Use sort() para que a lista seja armazenada em
ordem alfabética inversa. Imprima a lista para
mostrar que a ordem foi alterada.'''

country = ["Canadá", "USA", "Amsterdam", "Holanda", "Austrália", "Suiça"]
print(country)
print(sorted(country))
print(country)
print(sorted(country, reverse=True))
print(country)
country.reverse()
print(country)
country.reverse()
print(country)
country.sort()
print(country)
country.sort(reverse=True)
print(country)

print("\n----------------------------------------------")

print("Exercício 8.2\n")
'''Altere os códigos desenvolvidos para o convite de
jantar (Notas de aula 7) para que seja apresentada a
quantidade de convidados existentes.'''

names = ["joão", "marcela", "vanessa", "felipe"]

# Convites
names1 = names[:]
for names1 in names1:
  print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names1.title()))
print("\n")

# Informando que uma pessoa cancelou
print("O {} não estará presente no jantar que acontecerá no sábado!".format(names[3].title()))
print("\n")

# Adicionando a lista
names[3] = "thiago"
print("\n")

# Convites atualizados
names2 = names[:]
for names2 in names2:
  print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names2.title()))
print("\n")

# Informando que tem uma mesa maior
names3 = names[:]
print("Olá pessoal, agora nós temos uma mesa maior, e com isso mas pessoas estarão presente em nosso jantar!")
print("\n")

# Colocando pessoas na lista
names.insert(0, 'milena')
names.insert(3, 'geovana')
names.append('erick')

# Convites atualizados
names4 = names[:]
for names4 in names4:
  print("Olá {}, você está convidado para um jantar neste sábado as 20h.".format(names4.title()))
print("\n")

# Informando que algumas pessoas não vão poder ir ao jantar
print("No jantar de sábado só poderá ter 2 convidados presentes!")
print("\n")

# Tirando nomes da lista
names5 = names[:]
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
names_remove = names5.pop()
print("Olá {}, a mesa foi cancelada e agora a mesa disponível só tem 2 lugares, nosso jantar fica para um outro dia!".format(names_remove.title()))
print("\n")

print("Olá {}, você está convidado para o jantar".format(names[0].title()))
print("Olá {}, você está convidado para o jantar".format(names[1].title()))

# Zerando a Lista
del names5[0]
del names5[0]
print(names5)

# Mostrando quantos convites foram feitos
print("Convidados: {}".format(len(names)))

print("\n----------------------------------------------")

print("Exercício 8.3\n")
'''Pense em algo que você gostaria
armazenar em uma lista. Por exemplo, você pode criar uma
lista de montanhas, rios, países, cidades, idiomas ou
qualquer outra coisa que deseje. Escreva um programa
que crie uma lista contendo esses itens e, em seguida,
use cada função apresentada nesta nota pelo menos uma vez.'''

languages = ["Inglês", "Espanhol", "Japonês", "Árabe", "Russo", "Chines"]
print(languages)
print(sorted(languages))
print(languages)
print(sorted(languages, reverse=True))
print(languages)
languages.reverse()
print(languages)
languages.sort()
print(languages)
languages.sort(reverse=True)
print(languages)
print(len(languages))

print("\n----------------------------------------------")
