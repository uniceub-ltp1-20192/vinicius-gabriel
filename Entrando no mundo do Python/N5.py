print("Exercício 5.1\n")
'''Crie um programa que solicite um valor ao usuário e
informe se o mesmo é par ou impar.'''

number = int(input("Digite um número qualquer: "))
final = number % 2
if final == 0:
    print("O número {} é PAR".format(number))
if final == 1:
    print("O número {} é ÍMPAR".format(number))

print("\n----------------------------------------------")

print("Exercício 5.2\n")
'''Conceba uma programa que informe se um determinado
número é primo.'''

number = int(input("Digite um número: "))
total = 0
for counter in range(1, number + 1):
    if number % counter == 0:
        total += 1
print("O número {} foi divisível {} vezes.".format(number, total))

if total == 2:
    print("Esse número É PRIMO!")
if total != 2:
    print("Esse número NÃO É PRIMO!")

print("\n----------------------------------------------")

print("Exercício 5.3\n")
'''Crie uma calculadora automática que dado dois valores
informe o resultado da adição, subtração, multiplicação e
divisão.'''

number_1 = float(input("Digite um valor: "))
number_2 = float(input("Digite mais um valor: "))

addition = number_1 + number_2
subtraction = number_1 - number_2
multiplication = number_1 * number_2
division = number_1 / number_2

print("O valor da adição é {:.1f}".format(addition))
print("O valor da subtração é {:.1f}".format(subtraction))
print("O valor da multiplicação é {:.1f}".format(multiplication))
print("O valor da divisão é {:.1f}".format(division))

print("\n----------------------------------------------")

print("Exercício 5.4\n")
'''Crie uma programa que fornecidos a idade e o semestre
que um pessoa está informa em quantos anos ela vai se
formar.'''

age = int(input("Quantos anos você tem: "))
college = int(input("Quantos anos de duração do seu curso: "))
semester = int(input("Qual semestre você está cursando: "))
final = (college * 2 - semester) / 2
final_age = age + final
print("Falta {:.0f} anos para você se formar!".format(final))
print("Você vai terminar a faculdade com {:.0f} anos!".format(final_age))

print("\n----------------------------------------------")

print("Exercício 5.5\n")
'''Crie uma variação do programa 5.4 que receba também a
informação de quantos semestres o aluno está atrasado e
indique qual o tempo total que ele vai ter no curso até
se formar.'''

print("\n----------------------------------------------")
