print("Exercício 6.1\n")
'''Armazene os nomes de alguns de seus amigos em uma
lista chamada nomes. Imprima o nome de cada pessoa
acessando cada elemento da lista, um de cada vez.'''

names = ["sandra", "william", "elaine", "amanda", "guilherme", "gabriel"]
print("{}. \n{}. \n{}. \n{}. \n{}. \n{}.".format(names[0].title(), names[1].title(), names[2].title(), names[3].title(), names[4].title(), names[5].title()))

print("\n----------------------------------------------")

print("Exercício 6.2\n")
'''Comece com a lista que você criou no exercício 6.1,
mas em vez de apenas imprimir o nome de cada pessoa,
imprima uma mensagem para ela. O texto de cada mensagem
deve ser o mesmo, mas cada mensagem deve conter o nome da
pessoa específica.'''

names = ["sandra", "william", "elaine", "amanda", "guilherme", "gabriel"]
print("Olá, {}, tudo bem?. \nOlá, {}, tudo bem?. \nOlá, {}, tudo bem?. \nOlá, {}, tudo bem?. \nOlá, {}, tudo bem?. \nOlá, {}, tudo bem?.".format(names[0].title(), names[1].title(), names[2].title(), names[3].title(), names[4].title(), names[5].title()))

print("\n----------------------------------------------")

print("Exercício 6.3\n")
'''Pense no seu modo de transporte favorito, como uma
moto ou um carro, e faça uma lista que armazene modelos
do seu transporte favorito. Use sua lista para imprimir
uma série de mensagens sobre esses itens, como "Eu
gostaria de ter uma moto da Honda".'''

transport = ["Honda", "BMW", "Jetta", "Civic", "Maverick"]
print("Eu gostaria de ter um {}! \nEu gosto muito do {}! \nMeu sonho andar em uma {}! \nEu quero um {}! \nEu já andei em uma {}".format(transport[2].title(), transport[4].title(), transport[1].title(), transport[3].title(), transport[0].title()))

print("\n----------------------------------------------")
