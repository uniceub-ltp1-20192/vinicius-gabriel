# No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
# A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
# Descubra quem é o assassino sabendo que:
#        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é "u" ou "o", ele pode ser o assassino.
#        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
#        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
# Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

import sys

def askName():
    return input("Nome da pessoa investigada: ")

#Pedir um nome ao usuário
name = askName()

def validateStr(n):
    return name.split()

#Confere se o nome é uma String
nv = validateStr(name)

if not nv:
    sys.exit("TÁ ERRRRRAAAAAAADDDDDOOOO!!!")

def validateVgls(name):
    counter = 0
    for vgls in name:
        if vgls in "a, e, i":
            counter += 1
    return counter >= 3 and "o" not in name.lower() and "u" not in name.lower()

#Verifica se o nome respeita a RN1 (Vogais)
nv = validateVgls(name)

if not nv:
    sys.exit("Não tem chances de ser o ASSASSINO(A)!!!")

man = ["Renato Faca, Roberto Arma, Uesllei"]
woman = ["Maria Calice, Roberta Corda"]

def getBioGender(name):
    if name in man:
        return "Homem"
    if name in woman:
        return "Mulher"

list = ["Faca, Calice, Corda"]

def getWeapon(name):
    Murder = False
    for wpn in list:
        if wpn in name.lower():
            Murder = True
    return Murder

biogender = getBioGender(name)
weapon = getWeapon(name)

def validateAW(biogender, weapon):
    return biogender == "Mulher" and weapon

def validateAM(biogender, time):
    return biogender == "Homem"

vw = validateAW(biogender, weapon)
vm = validateAM

if vw and vm:
    sys.exit("Não tem chances de ser o ASSASSINO(A)!!!")

print("ASSASSINO(A)!!!")